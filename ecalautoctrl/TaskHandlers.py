import argparse
import subprocess
import logging
import inspect
from datetime import datetime
from typing import Optional, List, Dict, Type, Union, Any
from os import path
from itertools import islice
from .JobCtrl import JobCtrl
from .RunCtrl import RunCtrl
from .CMSTools import QueryDBS
from .tier0_api import T0GeneralAPI
from .processing_ctrl import list_campaigns
from ecalautoctrl.notifications import MattermostHandler

class LockBase:
    """
    Base class to implement a generic lock system to prevent handlers to
    process runs untill a predifined condition is met.
    """

    def __init__(self, **kwargs):
        # create the basic streamer logger
        self.stream_handler = logging.StreamHandler()
        self.stream_handler.setLevel(logging.INFO)
        self.stream_handler.setFormatter(logging.Formatter(f'[{str(self)}]: %(message)s'))
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger()
        self.log.addHandler(self.stream_handler)
        # remove default logger
        self.log.handlers.pop(0)


    def __str__(self):
        return 'LockBase'

    def set_logger(self, logger):
        """
        Set common logger
        """

        self.log = logger

    def lock(self, runs: List[Dict]):
        """
        Derived classes should implement the locking mechanisms in this
        method.

        :params runs: a list of runs in the :class:`~ecalautoctrl.RunCtrl` format.
        """

        raise NotImplementedError

class AutoCtrlScriptBase:
    """
    Base class providing a generic platform to build a script for the automation system.
    The base class provide the interface to  :class:`~ecalautoctrl.RunCtrl` and
    the notification system.
    It also implements the call method and the cmdline options skeleton.
    A derived class should implement subcommand options with an associated method that
    is executed by the :meth:`~AutoCtrlScriptBase.__call__` method.

    :param task: task name
    """

    def __init__(self, task: str, **kwargs):
        # task name as in the influxdb
        self.task = task
        # all upper case task name for the notification.
        self.appname = task.upper()

        # create the basic streamer logger
        self.stream_handler = logging.StreamHandler()
        self.stream_handler.setLevel(logging.INFO)
        self.stream_handler.setFormatter(logging.Formatter(f'[{self.appname}]: %(message)s'))
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger()
        self.log.addHandler(self.stream_handler)
        # remove default logger
        self.log.handlers.pop(0)

        self.parser = argparse.ArgumentParser(
            description="""
            Submit ECAL automation jobs.
            """, formatter_class=argparse.RawTextHelpFormatter)

        # common options
        self.parser.add_argument('--db',
                                 dest='dbname',
                                 default='ecal_online_test',
                                 type=str,
                                 help='Database name, default is test db')
        self.parser.add_argument('--notify',
                                 dest='notify',
                                 default=None,
                                 type=str,
                                 help='Mattermost incoming webhook url for notifications')
        self.parser.add_argument('--campaign',
                                 dest='campaign',
                                 nargs='+',
                                 type=str,
                                 help='Processing campaign(s). "all" for all campaigns in the db')
        self.parser.add_argument('--logurl',
                                 dest='logurl',
                                 default='https://ecallogs.web.cern.ch/',
                                 type=str,
                                 help='Base url for the logs')

    def __call__(self, *args, **kwargs):
        """
        Execute a generic command specified in the command line options as function associated to a subcommand.

        If multiple campaigns are specified with the -c/--campaign option the command is looped over all campaigns. If 'all' is specified the list of all campaigns available in the db specified with --db is loaded.
        """
        # parse the cmd line
        self.opts = self.parser.parse_args()

        # add the Mattermost logger if --notify is set
        if self.opts.notify:
            self.mm_handler = MattermostHandler(url=self.opts.notify)
            self.mm_handler.setFormatter(logging.Formatter(f'[{self.appname}]: %(message)s'))
            self.mm_handler.setLevel(logging.WARNING)
            self.log.addHandler(self.mm_handler)

        if 'cmd' in self.opts:
            # check for 'all' campaigns
            if 'all' in self.opts.campaign:
                self.opts.campaign = list_campaigns(self.opts.dbname, quite=True)

            # 'nargs' should always return a list, just in case wrap single string into a list
            status = 0
            if not isinstance(self.opts.campaign, list):
                self.opts.campaign = [self.opts.campaign]
            for cc in self.opts.campaign:
                self.campaign = cc
                self.log.info(f'Calling {self.opts.cmd.__name__} for campaign {self.campaign}')
                self.rctrl = RunCtrl(dbname=self.opts.dbname, campaign=self.campaign)
                ret = self.opts.cmd(*args, **kwargs)
                status += ret if ret else 0

            return status
        else:
            # cmd not found
            self.log.warning(f'Command {self.opts.cmd} not found')

            return None

    def cmdline_options(self):
        """Return the script cmdline options. This function is required to allow sphinx to build docoumentation for the script options."""
        return self.parser

    @staticmethod
    def export_options(cls):
        """
        Return the script cmdline options.

        Static version of `cmdline_options`. sphinx-argparse can only call a function without arguments.
        Here we need to first instantiate the class and then call `cmdline_options`.
        The static method is designed to work with any derived class, it constructs a dummy instance of the provided class
        and returns the command line options.

        :param cls: the class of which the command line arguments should be retrieved.
        """
        # this is not completely general, but should catch most of the cases
        args = {}
        params = inspect.signature(cls.__init__).parameters
        for k in params.keys():
            # add arguments for which default does not exist, skip self and kwargs
            if params.get(k).default == inspect._empty and k not in ['self', 'kwargs']:
                args[k] = 'dummy'
        inst = cls(**args)
        return inst.cmdline_options()


def get_files_dbs(self, runs: List[Dict]) -> List[str]:
    """
    Retrieve file names for given runs from DBS.
    This function is added by :deco:`~ecalautoctrl.dbs_data_source`.

    :param runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
    """
    flist = []
    try:
        if self.dsetname:
            dsets = [self.dsetname] if isinstance(self.dsetname, str) else self.dsetname
            for dset in dsets:
                for run in runs:
                    # add era to dataset name, validating it in the process
                    _, pd, sd, tier = dset.split('/')
                    sd = '*' + run["era"] + sd
                    fullname = f'/{pd}/{sd}/{tier}'
                    fromt0 = self.opts.t0 if 't0' in self.opts else False
                    lfn = self.opts.lfn if 'lfn' in self.opts else False
                    dataQuery = QueryDBS(dataset=fullname)
                    flist.extend(dataQuery.getRunFiles(run=int(run['run_number']), fromt0=fromt0, lfn=lfn))

        return flist
    except AttributeError:
        raise AttributeError('Please define self.dsetname in your class when using the dbs_data_source decorator')

def get_files_prev_task(self, runs: List[Dict]) -> List[str]:
    """
    Retrieve file names for given runs from a previous task.
    This function is added by :deco:`~ecalautoctrl.prev_task_data_source`.

    :param runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
    """
    flist = []
    try:
        # run on output of previous step
        if self.prev_input:
            flist.extend(self.rctrl.getOutput(runs=[run['run_number'] for run in runs],
                                              process=self.prev_input))
        return flist
    except AttributeError as ae:
        raise AttributeError('Please define self.prev_input in your class when using the dbs_data_source decorator: '+ae)

def get_sqlite_prev_task(self, runs: List[Dict]) -> List[str]:
    """
    Retrieve sqlite file path for given runs from a previous task.
    This function is added by :deco:`~ecalautoctrl.prev_task_sqlite_source`.

    :param runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
    """
    try:
        # run on output of previous step
        if self.prev_input:
            sqlite = self.rctrl.getField(process=self.prev_input, field='sqlite', runs=[run['run_number'] for run in runs])

        return sqlite
    except AttributeError as ae:
        raise AttributeError('Please define self.prev_input in your class when using the prev_task_sqlite_source decorator: '+ae)

# Decorators
def dbs_data_source(cls):
    """
    This decorator adds the a :func:`get_files` function that fetches data from dbs.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    The decorator requires the data member :attr:`dsetname` to be defined in the class.
    Usually this has to be set in the derived class :meth:`__init__`.
    """
    setattr(cls, 'get_files', get_files_dbs)
    return cls

def prev_task_data_source(cls):
    """
    This decorator adds the a :func:`get_files` function provides input files from a previous
    task recorded in the automation influxdb database.
    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    The decorator requires the data members :attr:`prev_input` to be defined in the class.
    Usually this has to be set in the derived class :meth:`__init__`.
    """
    setattr(cls, 'get_files', get_files_prev_task)
    return cls

def prev_task_sqlite_source(cls):
    """
    This decorator adds the a :func:`get_sqlite` function provides sqlite file path from a previous
    task recorded in the automation influxdb database.
    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    The decorator requires the data members :attr:`prev_input` to be defined in the class.
    Usually this has to be set in the derived class :meth:`__init__`.
    """
    setattr(cls, 'get_sqlite', get_sqlite_prev_task)
    return cls

def process_by_run(cls):
    """
    This decorator adds the a :func:`groups` function that implements a trivial splitting by run.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.
    """
    def groups(self) -> List[List[Dict]]:
        """
        Trivial run by run grouping.

        This function is added by :deco:`~ecalautoctrl.process_by_run`.
        """
        self.wdeps.update({self.task: 'new'})
        runs = self.rctrl.getRuns(status=self.wdeps)
        self.wdeps.update({self.task: 'reprocess'})
        runs.extend(self.rctrl.getRuns(status=self.wdeps))

        lock = self.lock(runs=runs)

        return [[run] for i, run in enumerate(runs) if not lock[i]]

    setattr(cls, 'groups', groups)
    return cls

def process_by_fill(fill_complete=False):
    """
    This decorator adds the a :func:`groups` function that implements splitting by fill.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    :param fill_complete: if set to `True` wait for the fill to be complete and all the data to
    be available. Otherwise return all the runs currently available in each fill. The second
    mode can be (which is the default) is useful to process data per fill in a "growing" dataset
    fashion.
    """
    def wrapper(cls):
        def groups(self) -> List[List[Dict]]:
            """
            Group runs by fill.

            This function is added by :deco:`~ecalautoctrl.process_by_fill`.
            """
            self.wdeps.update({self.task: 'new'})
            runs = self.rctrl.getRuns(status=self.wdeps)
            self.wdeps.update({self.task: 'reprocess'})
            runs.extend(self.rctrl.getRuns(status=self.wdeps))
            runs = sorted(runs, key=lambda r: r['run_number'])

            grps = []
            fills = {}
            for run in runs:
                if run['fill'] in fills:
                    fills[run['fill']].append(run)
                else:
                    fills[run['fill']] = [run]

            # remove fills for which previous step is not complete:
            # - check for fill that are ongoing
            # - check that the number of runs from getRuns equals the total number of runs
            #   in a given fill.
            if fill_complete:
                for fill, rr in fills.items():
                    if self.rctrl.fillDumped(fill=fill) and (len(self.rctrl.getRunsInFill(fill=fill, task=self.task, active=True))==len(rr)):
                        grps.append(rr)
            else:
                grps = list(fills.values())

            return grps

        setattr(cls, 'groups', groups)
        return cls
    return wrapper

def process_by_era():
    """
    This decorator adds the a :func:`groups` function that implements splitting by CMS data acquisition era.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.
    """
    def wrapper(cls):
        def groups(self) -> List[List[Dict]]:
            """
            Group runs by CMS data acquisition era.

            This function is added by :deco:`~ecalautoctrl.process_by_era`.
            """
            self.wdeps.update({self.task: 'new'})
            runs = self.rctrl.getRuns(status=self.wdeps)
            self.wdeps.update({self.task: 'reprocess'})
            runs.extend(self.rctrl.getRuns(status=self.wdeps))
            runs = sorted(runs, key=lambda r: r['run_number'])

            grps = []
            eras = {}
            for run in runs:
                if run['era'] in eras:
                    eras[run['era']].append(run)
                else:
                    eras[run['era']] = [run]

            grps = list(eras.values())

            return grps

        setattr(cls, 'groups', groups)
        return cls
    return wrapper

def process_by_intlumi(target: float=0., nogaps: bool=True):
    """
    This decorator adds the a :func:`groups` function that implements run splitting based on the amount of collected data.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    :param target: target recorded integrated luminosity (in /pb).
    :param nogaps: do not allow gaps between runs in a given group. This implies that only subsequent runs for which the
    task is injected will be used to build a group. If a dependency is not fulfilled for a run the group is not processed.
    Therefore the integrated luminosity target is computed on all runs injected in the automation,
    regardless of the dependency status. This ensures building reproducible groups (default behaviour).
    If the option is False groups are built to reach the desired lumi figure regardless of continuity.
    """
    def wrapper(cls):
        def groups(self) -> List[List[Dict]]:
            """
            Group runs to reach a desired integrated lumi.
            This function is added by :deco:`~ecalautoctrl.process_by_intlumi`.
            """
            # Groups are built from all runs injected for this task in case of no gaps. Dependencies in this case
            # are checked once the groups have been built.
            # Otherwise if gaps are allowed the dependencies are enforced at this stage.
            wdeps = {self.task: 'new'}
            if not nogaps:
                wdeps.update(self.wdeps)
            runs=self.rctrl.getRuns(status=wdeps)
            wdeps = {self.task: 'reprocess'}
            if not nogaps:
                wdeps.update(self.wdeps)
            runs.extend(self.rctrl.getRuns(status=wdeps))
            # sort by run number to create groups regardless if the tasks are in new or reprocess status
            runs = sorted(runs, key=lambda r: r['run_number'])

            grps = [[]]
            clumi = 0.
            for run in runs:
                clumi += run['lumi']
                grps[-1].append(run)
                # enough lumi, new group
                if clumi >= target:
                    grps.append([])
                    clumi = 0.

            # remove the last group which is the growing one
            if clumi < target:
                grps.pop()

            # check dependencies in case nogaps is set.
            if nogaps:
                self.wdeps.update({self.task: 'new'})
                available_runs = self.rctrl.getRuns(status=self.wdeps)
                self.wdeps.update({self.task: 'reprocess'})
                available_runs.extend(self.rctrl.getRuns(status=self.wdeps))
                available_runs = [r['run_number'] for r in available_runs]

                # remove groups that do not fulfill the dependencies
                grps = [grp for grp in grps if all(r['run_number'] in available_runs for r in grp)]

            return grps

        setattr(cls, 'groups', groups)
        return cls
    return wrapper

def process_by_nevents(targetEvents: int, dsetname: str="", applyMRHchecks = True):
    """
    This decorators add the a :func:`groups` function that implements run splitting based on the number of events
    collected in a specific dataset.
    Additional checks (specifically the MultiRunHarvesting requests) are applied by default, but can be switched off.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    :param targetEvents: minimum number of events needed to run the harvesting
    :param dsetname: name of the dataset for which the number of events collected are counted
    :param applyMRHchecks: apply or not the MRH requirements
    """
    def wrapper(cls):
        def groups(self) -> List[List[Dict]]:
            """
            Group runs to reach a desired number of events
            This function is added by :deco:`~ecalautoctrl.process_by_nevents`.
            """
            self.wdeps.update({self.task: 'new'})
            runs=self.rctrl.getRuns(status=self.wdeps)
            self.wdeps.update({self.task: 'reprocess'})
            runs.extend(self.rctrl.getRuns(status=self.wdeps))
            # sort by run number to create groups regardless if the tasks are in new or reprocess status
            runs = sorted(runs, key=lambda r: r['run_number'])

            # Define API factories
            dbs_api = QueryDBS()
            t0_api  = T0GeneralAPI()

            # Define outputs
            grps = [[]]
            cevents = 0

            prev_MRH_values = None

            # Loop on runs
            for run in runs:

                # Make MRH checks - FIXME: more checks (Bfield and dependent Record) should be added here
                if not prev_MRH_values and self.applyMRHchecks:
                    prev_MRH_values = t0_api.get_MRH_values(run=int(run['run_number']))

                if self.applyMRHchecks:
                    if prev_MRH_values == t0_api.get_MRH_values(run=int(run['run_number'])):
                        grps[-1].append(run)
                        cevents += dbs_api.getRunEvents(run=int(run['run_number']), dsetName=dsetname)
                    else:
                        grps.append([run])
                        cevents = dbs_api.getRunEvents(run=int(run['run_number']), dsetName=dsetname)
                        prev_MRH_values = t0_api.get_MRH_values(run=int(run['run_number']))
                else:
                    grps[-1].append(run)

                if cevents >= targetEvents:
                    grps.append([])
                    cevents = 0
                    prev_MRH_values = None

            # remove the last group which is the growing one
            grps.pop()

            return grps

        setattr(cls, 'groups', groups)
        setattr(cls, 'applyMRHchecks', applyMRHchecks)
        return cls
    return wrapper

class HandlerBase(AutoCtrlScriptBase):
    """
    Base class implementing the common structure for jobs (re)submisison.

    Derived classes should implement the :meth:`~ecalautoctrl.HandlerBase.submit`
    and :meth:`~ecalautoctrl.HandlerBase.resubmit` methods.
    A general :meth:`~ecalautoctrl.HandlerBase.check` method is provided.
    This base class adds also the cmdline options to select the basic
    commands that can be executed (submit, resubmit, check).
    Derived classes can extend the number of subcommands and/or add more
    options to each subcommand.
    This class should be combined with one of the data providers to achive
    task chaining and data grouping.

    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 deps_tasks: List[str] = None,
                 locks: List[Type[LockBase]] = None,
                 **kwargs):
        super().__init__(task=task, **kwargs)

        self.wdeps = {w: 'done' for w in deps_tasks} if deps_tasks else {}

        self.locks = locks if locks else []
        for lock in self.locks:
            lock.set_logger(self.log)

        self.parser.add_argument('--wdir',
                                 dest='wdir',
                                 default=None,
                                 type=str,
                                 help='Working directory')
        self.parser.add_argument('--eosdir',
                                 dest='eosdir',
                                 default=None,
                                 type=str,
                                 help='Base path of output location on EOS')

        # sub commands
        self.subparsers = self.parser.add_subparsers(dest='subcommand',
                                                     description='Select command to execute')

        # submit subcommand
        self.submit_parser = self.subparsers.add_parser('submit',
                                                        help='Process all runs marked as new in the automation db')
        self.submit_parser.set_defaults(cmd=self.submit)

        # mutually exclusive group
        exclargs = self.submit_parser.add_mutually_exclusive_group(required=False)
        exclargs.add_argument('--t0',
                              dest='t0',
                              default=False,
                              action='store_true',
                              help='Read input files from T0 storage')
        exclargs.add_argument('--lfn',
                              dest='lfn',
                              default=False,
                              action='store_true',
                              help='Use logical file names for input files')

        # resubmit subcommand
        self.resubmit_parser = self.subparsers.add_parser('resubmit',
                                                          help='Check for failed jobs and resubmit them')
        self.resubmit_parser.set_defaults(cmd=self.resubmit)

        self.resubmit_parser.add_argument('--lfn',
                                          dest='lfn',
                                          default=False,
                                          action='store_true',
                                          help='Use logical file names for input files')

        # check subcommand
        self.check_parser = self.subparsers.add_parser('check',
                                                       help='Check ongoing runs and mark them as done/failed if completed')
        self.check_parser.set_defaults(cmd=self.check)
        self.check_parser.add_argument('--max-retries',
                                       dest='max_retries',
                                       default=-1,
                                       type=int,
                                       help='Max number of tries, for each single job. -1 = no limit')
        self.check_parser.add_argument('--skipped-delay',
                                       dest='skipped_delay',
                                       default=7,
                                       type=int,
                                       help='Number of days after which a task is considered as stalled and is marked as skipped.')

    def lock(self, runs: List[Dict] = None) -> List[bool]:
        """
        Check all registered locks to prevent a run to be processed.

        Locks are set at initialization time and should take as inputs
        a list of dictionary (the `runs` parameter).

        :param runs: list of runs to be checked (:class:`~ecalautoctrl.RunCtrl` format).
        :return: a global decision if the group of runs are locked or not
        """
        locks = len(runs)*[False]
        for ilock in self.locks:
            if len(runs) > 0:
                for i, l in enumerate(ilock.lock(runs)):
                    if l:
                        self.log.info(f'run {runs[i]["run_number"]} held by {str(ilock)}')
                    locks[i] |= l

        return locks

    def get_files(self, runs: List[Dict] = None) -> List[str]:
        """
        Placeholder.

        Implementation left to derived classes and decorators.
        """
        raise NotImplementedError

    def groups(self) -> List[List[Dict]]:
        """
        Placeholder.

        Implementation left to derived classes and decorators.
        """
        raise NotImplementedError

    def submit(self):
        """
        Placeholder.

        Implementation left to derived classes
        """
        raise NotImplementedError

    def resubmit(self):
        """
        Placeholder.

        Implementation left to derived classes
        """
        raise NotImplementedError

    def check(self):
        """
        Check for completed runs.
        """

        # check runs in status new, reprocess, and processing.
        # check 'new' and 'reprocess' to make sure idle runs (missing data, permanently locked)
        # get eventually flagged as "skipped".
        for sts in ['processing', 'new', 'reprocess']:
            runs = self.rctrl.getRuns(status={self.task: sts}, active=True)

            for run_dict in runs:
                run_sts = {}
                run = run_dict['run_number']
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run, 'fill': run_dict['fill']},
                            dbname=self.opts.dbname)
                delay = (datetime.utcnow() -
                         self.rctrl.getRunStatusChangeTime(run=run, status=(self.task, sts))).days

                # run fully processed: either task completed or run has been in status "processing"
                # for more than a week (i.e. most likely short run with no data to process).
                if sts == 'processing' and jctrl.taskCompleted():
                    run_sts.update({self.task: 'done'})
                    self.log.info(f'Task {self.task} for run {run} completed')
                elif (not jctrl.taskExist() or sts == 'reprocess') and delay>self.opts.skipped_delay:
                    run_sts.update({self.task: 'skipped'})
                    self.log.info(f'Task {self.task} for run {run} skipped')
                elif sts == 'processing' and self.opts.max_retries > -1:
                    failed = jctrl.getFailed()
                    if failed and min([jctrl.getNRetries(jid=job) for job in failed])>self.opts.max_retries:
                        run_sts.update({self.task: 'failed'})
                        logfiles = '\n'.join([jctrl.getJob(jid=job, last=True)[-1]['log'] if jctrl.getJob(jid=job, last=True)[-1]['log'] else '' for job in failed])
                        self.log.error(f'Task {self.task} for run {run} permanently marked as failed. \n Please check the logs: {logfiles}')

                # update the run status
                self.rctrl.updateStatus(run=run, status=run_sts)
                # update the status of the other runs merged with the master one
                if len(run_sts)>0 and jctrl.taskExist():
                    group = jctrl.getJob(jid=0, last=True)[-1]['group'] if 'group' in jctrl.getJob(jid=0, last=True)[-1] else ''
                    if group:
                        for r in group.split(','):
                            self.rctrl.updateStatus(run=r, status=run_sts)

class HTCHandler(HandlerBase):
    """
    HTCondor jobs handler for ECAL automation.

    This class provide a convenient and standard interface for worflows
    in the ECAL automation that submits batch jobs through HTCondor.
    It implements the relevant :meth:`~ecalautoctrl.HTCHandler.submit`
    and :meth:`~ecalautoctrl.HTCHandler.resubmit` methods. It should
    be coupled with a data provider class to get the input data and perform
    the run grouping.

    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self, task: str, deps_tasks: List[str] = None, **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        # submit additional options
        self.submit_parser.add_argument('--nfiles',
                                        dest='nfiles',
                                        default=None,
                                        type=int,
                                        help='Number of files per job, None="all"')
        self.submit_parser.add_argument('--template',
                                        dest='template',
                                        default='template.sub',
                                        type=str,
                                        help='HTCondot template submit file')
        # resubmit additional options
        self.resubmit_parser.add_argument('--template',
                                          dest='template',
                                          default='template.sub',
                                          type=str,
                                          help='HTCondot template submit file')
        self.resubmit_parser.add_argument('--resub-flv',
                                          dest='resubflv',
                                          default='workday',
                                          type=str,
                                          help='Resubmit JobFlavour')

    def check_running_job(self, jid: str) -> bool:
        """
        Check if specified job is still running.

        :param jid: ClusterId.ProcId
        :return: True if job is still running.
        """
        return subprocess.run(f'condor_q {jid} -run -json', shell=True, capture_output=True).stdout != b''

    def submit(self):
        """
        Submit runs in status task: new.
        Resubmit runs in status task: reprocess.
        """
        scheduler_name_warning_emitted = False
        for group in self.groups():
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                # allow reprecessing only if previous jobs are not running
                allow_repr = (run[self.task] == 'reprocess')
                if jctrl.taskExist() and run[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        self.log.info(f"Reprocessing of run {run['run_number']} not allowed because {len(jobs['idle']+jobs['running'])} job(s) are in 'idle' or 'running' state.")
                        allow_repr = False
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'
                    # split the file list into groups accordingly to nfiles,
                    # apexes are added around join(f) to ensure condor will later
                    # interpret the comma separated list as a single item
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    nfiles = self.opts.nfiles if self.opts.nfiles else len(fdict)
                    it = iter(fdict)
                    flist = [",".join([f for f in ff]) for ff in iter(lambda: tuple(islice(it, nfiles)), ())]
                    with open('args.txt', 'w') as ff:
                        ff.write('\n'.join([f for f in flist]))
                    # get the global tag from the influxdb.
                    # if not set leave GT empty (rely on auto cond).
                    gt = run['globaltag'] if 'globaltag' in run else '\'\''
                    self.log.info(f'GlobalTag: {gt}')
                    # get the cert JSON file path from the influxdb.
                    # if not set leave empty
                    certjson = run['certjson'] if 'certjson' in run and run['certjson'] != None else '\'\''
                    self.log.info(f'Certified runs JSON file path: {certjson}')
                    ret = subprocess.run(['condor_submit',
                                          f'{self.opts.template}',
                                          '-spool',
                                          '-queue', '1 fname from args.txt',
                                          '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {certjson}"'],
                                         capture_output=True)
                    #remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info('Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = self.opts.logurl
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - log file
                        try:
                            ret2 = subprocess.run(['myschedd', 'show'],
                                                capture_output=True, text=True)
                            if ret2.returncode == 0:
                                myschedd = next(line.split(': ')[1] for line in result.stdout.split('\n') if line.startswith('Current schedd:'))
                                if not myschedd.endswith('.cern.ch'):
                                    self.log.warn("Unexpected scheduler name pattern:"+ret2.stdout+"\nFallback to empty string")
                                    myschedd = ""
                            else:
                                if not scheduler_name_warning_emitted:
                                    self.log.warn("Failed to obtain scheduler name. Error msg:"+ret2.stderr+"\nFallback to empty string")
                                    scheduler_name_warning_emitted = True
                                myschedd = ""
                        except Exception as ex:
                            if not scheduler_name_warning_emitted:
                                self.log.error(f'{type(ex).__name__}:\n{ex}')
                                self.log.warn("Failed to obtain scheduler name. Fallback to empty string")
                                scheduler_name_warning_emitted = True
                            myschedd = ""
                        try:
                            jctrl.createTask(jids=list(range(len(flist))),
                                             recreate=allow_repr,
                                             fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                      'inputs': f,
                                                      'htc-schedd':f"{myschedd}",
                                                      'htc-id': f"{cluster}.{i}",
                                                      'log': f'{logurl}/{self.task}-{cluster}-{i}.log'} for i,f in enumerate(flist)])
                        except Exception as ex:
                            self.log.error(f'{type(ex).__name__}:\n{ex}')
                            self.log.warning('Automation JobCtrl failed to create task for run(s): ' + ','.join([r['run_number'] for r in group]) + f'. Marking all jobs of cluster {cluster} for removal.')
                            self.log.info(subprocess.run(['condor_rm', f'{cluster}'], capture_output=True).stdout.decode().strip())
                            return -1

                        # set the master run to status processing
                        self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                    else:
                        self.log.warn("Failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1
        return 0

    def resubmit(self):
        """
        Query for failed jobs and resubmit.
        """

        runs = self.rctrl.getRuns(status={self.task: 'processing'})

        for run_dict in runs:
            run = run_dict['run_number']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run, 'fill': run_dict['fill']},
                            dbname=self.opts.dbname)
            # check for evicted jobs. Jobs still marked as running in the db but not
            # actually running in condor.
            for jid in jctrl.getRunning():
                if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                    jctrl.failed(jid=jid)

            # resubmit failed
            failedjobs = jctrl.getFailed()
            if not jctrl.taskExist() or failedjobs == []:
                self.log.info(f'no failures found for run {run}')
            else:
                self.log.info(f'found {len(failedjobs)} failed job(s) for run {run}')
                # Building the resubmission arguments
                # jobid, files
                # where jobid is the jobid of the original submission
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                # move to AAA or optionally LFN if file can't be read from T0 eos.
                flist = []
                for i in failedjobs:
                    inputs = jctrl.getJob(i, last=True)[0]['inputs']
                    if not all([path.isfile(p) for p in inputs.split(',')]):
                        t0prefix = 'file:/eos/cms/tier0'
                        xrdprefix = 'root://cms-xrd-global.cern.ch/'
                        lfn = self.opts.lfn if 'lfn' in self.opts else False
                        if lfn:
                            inputs = inputs.replace(t0prefix, '').replace(xrdprefix, '')
                        else:
                            inputs = inputs.replace(t0prefix, xrdprefix)
                    flist.append(i+', '+inputs)
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'
                with open('args.txt', 'w') as ff:
                    ff.write('\n'.join(flist))
                # get the global tag from the influxdb.
                # if not set leave GT empty (rely on auto cond).
                gt = run_dict['globaltag'] if 'globaltag' in run_dict else '\'\''
                self.log.info(f'GlobalTag: {gt}')
                # get the cert JSON file path from the influxdb.
                # if not set leave empty
                certjson = run_dict['certjson'] if 'certjson' in run_dict and run_dict['certjson'] != None else '\'\''
                self.log.info(f'Certified runs JSON file path: {certjson}')
                ret = subprocess.run(['condor_submit',
                                      f'{self.opts.template}',
                                      '-spool',
                                      '-queue', 'jobid, fname from args.txt',
                                      '-append', f'arguments = "$(ClusterId).$(ProcId) $(jobid) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {certjson}"',
                                      '-append', f'+JobFlavour = "{self.opts.resubflv}"'],
                                     capture_output=True)
                #remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    cluster = ret.stdout.decode().strip().split()[-1][:-1]
                    logurl = self.opts.logurl
                    # set the job status to idle and update the htc-id and log file path
                    for i, jid in enumerate(failedjobs):
                        jctrl.idle(jid=jid, fields={'htc-id': f"{cluster}.{i}", 'log': f'{logurl}/{self.task}-{cluster}-{i}.log'})
                else:
                    self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                    return -1
        return 0


@dbs_data_source
@process_by_run
class HTCHandlerByRunDBS(HTCHandler):
    """
    HTCHandler to process data run by run, reading data from DBS.
    The acquisition era is automatically adjusted based on the run number in
    the dataset name.

    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    :param dsetname: central dataset name(s) in the three fields format (/prim/sec/tier, wildcards allowd)
    """

    def __init__(self,
                 task: str,
                 dsetname: Union[str, List[str]],
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.dsetname = dsetname

@prev_task_data_source
@process_by_run
class HTCHandlerByRun(HTCHandler):
    """
    HTCHandler to process data run by run, reading data from DBS.
    The acquisition era is automatically adjusted based on the run number in
    the dataset name.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if deps_tasks is None:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

@prev_task_data_source
@process_by_fill(fill_complete=True)
class HTCHandlerByFill(HTCHandler):
    """
    HTCHandler to process data fill by fill, only once a fill is completed
    and all the data from the previous steps are available.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if deps_tasks is None:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

@prev_task_data_source
@process_by_fill(fill_complete=False)
class HTCHandlerGrowingFill(HTCHandler):
    """
    HTCHandler to process data fill by fill, allowing data to be processed as fill is
    ongoing.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if deps_tasks is None:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

class ScriptHandler(HandlerBase):
    """
    Jobs handler to run script locally
    """

    def _init__(self, task: str, deps_tasks: List[str] = None, **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.submit_parser.add_argument('--script',
                                dest='script',
                                default='script.sh',
                                type=str,
                                help='Executable script')

    def submit(self):
        for group in self.groups():
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                allow_repr = (run[self.task] == 'reprocess')
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    gt = run['globaltag'] if 'globaltag' in run else '\'\''
                    self.log.info(f'GlobalTag: {gt}')
                    self.log.info(f'Launching job for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                    ret = subprocess.run([f'{self.opts.script}', fdict, eosdir, self.opts.wdir, gt
                                          ], capture_output=True)
                    self.log.info(ret.stdout.decode().strip())
                    if ret.returncode == 0:
                        self.log.info(ret.stderr.decode())
                        jctrl.done(jid=0)
                    else:
                        self.log.error("failed to run script. Error msg:"+ret.stderr.decode())
                        jctrl.failed(jid=0)
        return 0

    def resubmit(self):
        runs = self.rctrl.getRuns(status={self.task:'processing'})

        for run_dict in runs:
            run = run_dict['run_number']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run, 'fill': run_dict['fill']},
                            dbname=self.opts.dbname)
            if not jctrl.taskExist() or jctrl.getFailed() == []:
                self.log.info(f'no failures found for run {run}')
            else:
                self.log.info(f'found failed job for run {run}')
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                # move to AAA if file can't be read from T0 eos.
                flist = jctrl.getJob(jctrl.getFailed()[0],last=True)[0]['inputs']
                if '/eos/cms/tier0' in flist and not all([path.isfile(p) for p in flist.split(',')]):
                    flist = flist.replace('file:/eos/cms/tier0', 'root://cms-xrd-global.cern.ch/')

                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'
                gt = run_dict['globaltag'] if 'globaltag' in run_dict else '\'\''
                self.log.info(f'GlobalTag: {gt}')
                ret = subprocess.run([f'{self.opts.script}', flist, eosdir, self.opts.wdir, gt
                                          ], capture_output=True)
                self.log.info(ret.stdout.decode().strip())
                if ret.returncode == 0:
                    self.log.info(ret.stderr.decode())
                    jctrl.done(jid=0)
                else:
                    self.log.error("failed to run script. Error msg:"+ret.stderr.decode())
                    jctrl.failed(jid=0)
        return 0


def iterate_by_number(cls):
    """
    This decorator adds a :func:`should_iterate` function that implements an iteration condition
    by reaching a certain maximum number of iterations.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.IterationBaseHandler`.
    """
    def should_iterate(self) -> bool:
        """
        Stop the iteration when a certain number of iteration is reached.

        This function is added by :deco:`~ecalautoctrl.iterate_by_number`.
        """
        return self.curr_iteration < self.max_iterations

    setattr(cls, 'should_iterate', should_iterate)
    return cls


class IterationBaseHandler(AutoCtrlScriptBase):
    """
    Base class implementing the common structure for iteration check.

    Derived classes should implement the :meth:`~ecalautoctrl.IterationBaseHandler.should_iterate` method.
    A general :meth:`~ecalautoctrl.IterationBaseHandler.check` method is provided.
    Derived classes can extend the number of subcommands and/or add more
    options to each subcommand.

    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    :param tasks_to_iterate: list of tasks which might need further iteration.
    """
    def __init__(self,
                 task: str,
                 deps_tasks: List[str],
                 tasks_to_iterate: List[str],
                 **kwargs):
        super().__init__(task=task, **kwargs)

        self.deps_tasks = deps_tasks
        self.tasks_to_iterate = tasks_to_iterate

        self.curr_iteration = -1

        self.subparsers = self.parser.add_subparsers(dest='subcommand',
                                                     description='Select command to execute')
        self.check_parser = self.subparsers.add_parser('check',
                                                       help='Check ongoing runs and submit them for reprocessing if iteration is needed')
        self.check_parser.set_defaults(cmd=self.check)

    def should_iterate(self) -> bool:
        """
        Placeholder.

        Implementation left to derived classes and decorators.
        """
        raise NotImplementedError

    def check(self):
        """
        Check for runs for which tasks might need futher iterations. If such exist, mark them for reprocessing.
        """

        runs: List[Dict[str, Any]] = self.rctrl.getRuns(status={}, active=True)
        for run in runs:
            run_number: int = run['run_number']
            iteration_db_field_name: str = RunCtrl.getIterationDbFieldName(self.task)
            self.curr_iteration: int = run[iteration_db_field_name]
            self.log.info(f'Current iteration: {self.curr_iteration}')

            are_all_deps_tasks_done: bool = all(run[dep_task] == 'done' for dep_task in self.deps_tasks)
            if are_all_deps_tasks_done:
                if self.should_iterate():
                    next_iteration: int = self.curr_iteration + 1
                    fields_to_update: Dict[str, Union[str, int]] = {task_to_iterate: 'reprocess' for task_to_iterate in self.tasks_to_iterate}
                    fields_to_update.update({iteration_db_field_name: next_iteration})
                    fields_to_update.update({self.task: 'processing'})
                    self.rctrl.updateStatus(run=run_number, status=fields_to_update)
                    self.log.info(f'Iteration {next_iteration} requested. Tasks to iterate marked for reprocessing: {self.tasks_to_iterate}')
                else:
                    self.rctrl.updateStatus(run=run_number, status={self.task: 'done'})
                    self.log.info(f'Iteration stop condition reached for tasks: {self.tasks_to_iterate}.')

            else:
                self.log.info(f"Not all task dependencies from {self.deps_tasks} are in state 'done'. No iteration check needed yet.")


@iterate_by_number
class IterationByNumberHandler(IterationBaseHandler):
    """
    Iteration handler with a stop condition when a certain number of iterations has been reached.

    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    :param tasks_to_iterate: list of tasks which might need further iteration.
    :param max_iterations: maximum number of iterations (the stop condition).
    """
    def __init__(self,
                 task: str,
                 deps_tasks: List[str],
                 tasks_to_iterate: List[str],
                 max_iterations: int,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, tasks_to_iterate=tasks_to_iterate, **kwargs)
        self.max_iterations = max_iterations
