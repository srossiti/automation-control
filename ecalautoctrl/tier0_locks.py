'''
This module provide an interface to the T0 monitoring pages that
do not support HTTPS GET queries and needs to be parsed manually.
Locks are implemented to wait for full processing of Repack/Express/Prompt
'''

import requests
import fnmatch
from typing import List, Dict, Optional, Union, Tuple
from .TaskHandlers import LockBase
from .tier0_api import T0GeneralAPI
from .CMSTools import QueryOMS

class T0ProcDatasetLock(LockBase):
    """
    This class provides a lock mechanism based on each run processing
    status at T0. Given a dataset or stream name the lock method checks for
    processing being completed for the given dataset primary dataset or stream and
    the specified processing stage (Repack, Express, PromptReco).

    :param dataset: dataset name. Either full (including wildcards) or pd/stream only (/EGamma, /EGamma/Run2022*/RAW-RECO, etc).
    :param stage: Repack, Express, PromptReco. If omitted check all.
    """

    def __init__(self, dataset: str, stage: Optional[str]=None, **kwargs):
        super().__init__()

        self.valid_stages = ['Repack', 'Express', 'PromptReco']

        self.stage = None
        # check validity of specified stage
        if stage:
            self.stage = [s for s in self.valid_stages if s.casefold() == stage.casefold()]
        # in case of invalid stage or omitted stage keep all
        if not self.stage:
            self.stage = self.valid_stages
            self.log.warning(f'T0ProcDatasetLock - {stage} is not a valid T0 stage. Specify one among Repack, Express, PromptReco')

        # is it a stream or a PD?
        self.stream = [s in ['Repack', 'Express'] for s in self.stage]

        # keep only the primary dataset name
        self.pd = dataset.split('/')[1] if '/' in dataset else dataset

        self.log.info(f"T0ProcDatasetLock - will check status for '{self.pd}' at stage(s) '{','.join(self.stage)}'")

    def __str__(self):
        return 'T0ProcDatasetLock'

    def lock(self, runs: List[Dict]) -> List[bool]:
        """
        Check if processing of specified stage+dataset is marked completed by T0.

        :params runs: a list of runs in the :class:`~ecalautoctrl.RunCtrl` format.
        :return: the corresponding lock status. 
        """
        t0genapi = T0GeneralAPI()
        qoms = QueryOMS()

        locks = len(runs)*[True]
        for i,r in enumerate(runs):
            # get requested PD or stream matched to available PDs or streams
            run_number = r["run_number"]
            if 'PromptReco' in self.stage:
                all_pds = qoms.get_datasets(run_number)
                matched_pds = [pd for pd in all_pds if fnmatch.fnmatch(pd.casefold(), self.pd.casefold())]
            if len({'Repack', 'Express'}.intersection(self.stage)) > 0:
                all_streams = qoms.get_streams(run_number)
                matched_streams = [s for s in all_streams if fnmatch.fnmatch(s.casefold(), self.pd.casefold())]

            stagedone = len(self.stream) * [False]
            for j,st in enumerate(self.stream):
                # check if a stream is repacked or a PD is processed
                if st:
                    if len(matched_streams) > 0:
                        stagedone[j] = all([t0genapi.get_stream_done(run_number, pd) for pd in matched_streams])
                else:
                    if len(matched_pds) > 0:
                        stagedone[j] = all([t0genapi.get_dataset_done(run_number, pd) for pd in matched_pds])

            locks[i] = not all(stagedone)

        return locks

class T0FCSRLock(LockBase):
    """
    This class provides a lock mechanism based on the first condition safe run (FCSR)
    at T0. The lock method checks if the run number to be processed is smaller than the FCSR to release the lock.
    """

    def __init__(self):
        super().__init__()

    def __str__(self):
        return 'T0FCSRLock'

    def lock(self, runs: List[Dict]) -> List[bool]:
        """
        Check if the run number to be processed is smaller than the run number of the FCSR at the T0.

        :params runs: a list of runs in the :class:`~ecalautoctrl.RunCtrl` format.
        :return: the corresponding lock status.
        """
        t0genapi = T0GeneralAPI()

        fcsr = int(t0genapi.get_fcsr())
        locks = len(runs)*[True]
        for i,r in enumerate(runs):
            # release the lock (i.e. return False) if the run number is smaller than the FCSR
            locks[i] = int(r["run_number"]) >= fcsr

        return locks
