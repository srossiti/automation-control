'''
This module provide an general interface to the T0 API.
'''

import requests
import logging
import os
import sys
from typing import Dict

class T0GeneralAPI():
    """
    This class provides access to the 'standard' T0 API which is accessible
    via HTTPS queries.
    """

    def __init__(self):
        self.t0_url = f'https://cmsweb.cern.ch/t0wmadatasvc/prod/'
        self.x509_path = os.getenv('X509_USER_PROXY')
        # check if the proxy certificate file is available (does not check if it is valid)
        if self.x509_path is not None:
            if not os.path.isfile(self.x509_path):
                logging.error(f"X509_USER_PROXY environment variable does not point to an existing file. X509_USER_PROXY: '{self.x509_path}'")
                sys.exit(1)
        else:
            logging.error('X509_USER_PROXY environment variable is not set.')
            sys.exit(1)

    def __str__(self):
        return 'T0GeneralAPI'

    def get_MRH_values(self, run: int) -> Dict[str,str]:
        """
        Returns a dictionary of values of the express processing for the specified run.

        :param run:  CMS run number.
        """
        # Return object
        MRH_values = {}

        # Compose URL
        target_url = self.t0_url + 'express_config?run=' + str(run)

        # Query
        resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        # Parse
        if len(resp):
            for dset in resp:
                MRH_values['cmssw']      = dset['cmssw']
                MRH_values['scram_arch'] = dset['scram_arch']
                MRH_values['global_tag'] = dset['global_tag']
        else:
            # if not available for the current run get the value
            # from the current run under processing in T0
            resp = requests.get(url=self.t0_url + '/express_config', verify=False, cert=self.x509_path).json()['result']
            if len(resp):
                for dset in resp:
                    MRH_values['cmssw']      = dset['cmssw']
                    MRH_values['scram_arch'] = dset['scram_arch']
                    MRH_values['global_tag'] = dset['global_tag']

        return MRH_values

    def get_gt(self, run: int, rtype: str='prompt') -> str:
        """
        Return the Prompt/Express GT as string for the specified run.

        :param run:  CMS run number.
        :param rtype: reco type: prompt or express.
        """
        if rtype not in ['prompt', 'express']:
            logging.error(f'Specified reco ({rtype}) type not supported. Please choose prompt or express')
            sys.exit(1)

        config_name = 'reco_config' if rtype=='prompt' else 'express_config'
            
        # Return value
        t0gt = ''

        # Compose URL
        target_url = self.t0_url + config_name + '?run=' + str(run)

        # Query
        resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        # Parse
        if len(resp):
            for dset in resp:
                if 'ppEra' in dset['scenario']:
                    t0gt = dset['global_tag']
        else:
            # if global_tag not available for the current run get the value
            # from the current run under processing in T0
            resp = requests.get(url=f'{self.t0_url}/{config_name}', verify=False, cert=self.x509_path).json()['result']
            t0gt = resp[0]['global_tag']

        return t0gt

    
    def get_prompt_gt(self, run: int) -> str:
        """
        Return the Prompt GT as string for the specified run.

        :param run:  CMS run number.
        """
        return self.get_gt(run=run, rtype='prompt')

    def get_fcsr(self) -> str:
        """
        Return the first condition safe run, that is the run which will be assigned to a new prompt condition payload IOV.

        :return: run number
        """
        # Compose URL
        target_url = self.t0_url + 'firstconditionsaferun'

        # Query
        resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        if len(resp) > 0:
            return resp[0]

        return '1'

    def get_datasets(self, run: int) -> list[str]:
        """
        Get the defined primary datasets for a run.

        :return: list of primary datasets
        """
        # Compose URL
        target_url = self.t0_url + f'reco_config?run={run}'

        # Query
        resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        datasets = []

        if len(resp) > 0:
            for dset in resp:
                datasets.append(dset['primary_dataset'])

        return datasets

    def get_express_streams(self, run: int) -> list[str]:
        """
        Get the defined express streams for a run.

        :return: list of express streams
        """
        # Compose URL
        target_url = self.t0_url + f'express_config?&run={run}'

        # Query
        resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        datasets = []

        if len(resp) > 0:
            for dset in resp:
                datasets.append(dset['stream'])

        return datasets

    def get_stream_done(self, run: int, stream: str) -> bool:
        """
        Get the stream done status for a run.

        :return: True if the stream is in done status for the run
        """
        # Compose URL
        target_url = self.t0_url + f'run_stream_done?&run={run}&stream={stream}'

        # Query
        resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        if len(resp) > 0:
            return resp[0]

        logging.error(f"No '{stream}' stream status result received for run {run}.")
        return False

    def get_dataset_done(self, run :int, dset: str) -> bool:
        """
        Get the dataset done status for a run.

        :return: True if the dataset is in done status for the run
        """
        # Compose URL
        target_url = self.t0_url + f'run_dataset_done?run={run}&primary_dataset={dset}'

        # Query
        resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        if len(resp) > 0:
            return resp[0]

        logging.error(f"No '{dset}' dataset status result received for run {run}.")
        return False

    def get_acq_era(self, run: int) -> str:
        """
        Get the acquisition era for a run. If acq_era not available for the run get the value from the current run being processed at the T0

        :param run:  CMS run number.
        :return: Acquisition era for the run or the current run being processed at the T0
        """
        # Compose URL
        target_url = self.t0_url + 'run_config'

        # Query
        resp = requests.get(url=target_url + f'?run={run}', verify=False, cert=self.x509_path).json()['result']

        if len(resp) == 0:
            # if acq_era is not available for the requested run get the value from the
            # current run being processed at the T0
            resp = requests.get(url=target_url, verify=False, cert=self.x509_path).json()['result']

        return resp[-1]['acq_era']

