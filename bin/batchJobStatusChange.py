#!/usr/bin/env python3

# script to rectify wrong idle or running job status in the DB in order to allow reprocesing of runs

import argparse
import subprocess
from sys import exit
from typing import Optional
from ecalautoctrl import RunCtrl, JobCtrl

def cmdline_options():
    """
    Return the argparse instance to handle the cmdline options for this script.

    The function is needed by sphinx-argparse to easily generate the docs.
    """
    parser = argparse.ArgumentParser(description="""
    Rectify wrong idle or running job status in the DB in order to allow reprocesing of runs
    """, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--db',
                        dest='dbname',
                        default='ecal_online_test',
                        type=str,
                        help='Database name, default is test db')

    parser.add_argument('-c', '--campaign',
                        dest='campaign',
                        default='prompt',
                        required=True,
                        type=str,
                        help='Processing campaign')

    parser.add_argument('--tasks',
                        dest='tasks',
                        default=None,
                        type=str,
                        nargs='+',
                        help='Which workflows to replay, space separated, default all')

    parser.add_argument('--taskstatus',
                        dest='taskstatus',
                        default='failed',
                        type=str,
                        help='Task status for which to update job status')

    parser.add_argument('--jobstatus',
                        dest='jobstatus',
                        default='failed',
                        type=str,
                        help='Job status to set idle or running jobs to')

    parser.add_argument('--range',
                        dest='rrange',
                        default='',
                        type=lambda opt: opt.split(',') if opt else [None, None],
                        help='Define a range of runs to be checked (min,max).')

    parser.add_argument('--condorrm', action='store_true')

    parser.add_argument('--schedd',
                        dest='schedd',
                        default=None,
                        type=str,
                        help='HTCondor scheduler to query for htc-ids')

    return parser

def set_job_status(jctrl, job, status):
    if status == 'failed':
        jctrl.failed(job)
    elif status == 'done':
        jctrl.done(job)

def check_condor_auth() -> bool:
    """
    Check if authentication tokens to access condor are available.

    :return: True if job is still listed on condor.
    """
    stderr = subprocess.run(f'condor_q', shell=True, capture_output=True).stderr
    if stderr != b'':
        print(stderr)
        return False
    return True

def check_condor_job(jid: str, schedd: Optional[str]=None) -> bool:
    """
    Check if specified job is still on condor.

    :param jid: ClusterId.ProcId
    :param schedd HTCondor scheduler to query
    :return: True if job is still listed on condor.
    """
    scheddopt = f" -name {schedd}" if schedd is not None else ''

    exists = False
    exists |= subprocess.run(f'condor_q {jid}{scheddopt} -run -json', shell=True, capture_output=True).stdout != b''
    exists |= subprocess.run(f'condor_q {jid}{scheddopt} -hold -json', shell=True, capture_output=True).stdout != b''
    exists |= subprocess.run(f'condor_q {jid}{scheddopt} -idle -json', shell=True, capture_output=True).stdout != b''
    return exists

def remove_condor_job(jctrl, job: str, schedd: Optional[str]=None):
    """
    Mark a HTCondor job for removal.

    :param job: ClusterId.ProcId
    :param schedd HTCondor scheduler to query
    """
    htcid = jctrl.getJob(job, last=True)[0]['htc-id']
    if htcid != None and check_condor_job(htcid, schedd=schedd):
        print(f"Marking HTCondor job id {htcid} for removal")
        scheddopt = f" -name {schedd}" if schedd is not None else ''
        subprocess.run(f'condor_rm{scheddopt} {htcid}', shell=True, capture_output=True)

if __name__ == '__main__':
    opts = cmdline_options().parse_args()

    dbname = opts.dbname
    campaign = opts.campaign
    tasks = opts.tasks
    taskStatus = opts.taskstatus
    destJobStatus = opts.jobstatus
    schedd = opts.schedd
    condorrm = opts.condorrm
    rrange = opts.rrange
    if len(rrange) != 2:
        print(f'--range requires exactly 2 values (comma separated). {len(rrange)} provided.')
        exit(-1)

    # Check access to condor
    if condorrm and not check_condor_auth():
        print("No access to HTCondor. Do you have authentication tokens? Try to run kinit '[user]@CERN.CH' to obtain a Kerberos ticket for the user that runs the jobs. Terminating.")
        exit(-1)

    rctrl = RunCtrl(dbname=dbname, campaign=campaign)
 
    for task in tasks:
        print(f'Setting job status to {destJobStatus} for {taskStatus} runs in task {task}.')
        srcRuns = rctrl.getRuns({task: taskStatus})
 
        fills = {}
        for it in srcRuns:
            runnumber = int(it['run_number'])
            appendrun = True
            if rrange != [None, None]:
                rrange.sort()
                if runnumber < int(rrange[0]) or runnumber > int(rrange[1]):
                    appendrun = False
            if appendrun:
                if it['fill'] in fills:
                    fills[it['fill']].append(runnumber)
                else:
                    fills[it['fill']] = [runnumber]
        print(fills)
 
        for fill in fills.keys():
            for run in fills[fill]:
                print(f'Processing run {run} of fill {fill}')
                jctrl = JobCtrl(task=task, tags={'run_number':run, 'fill':fill}, dbname=dbname, campaign=campaign)
                jobs = jctrl.getJobs()

                if len(jobs['idle']) > 0:
                    print(f"Setting {len(jobs['idle'])} jobs with 'idle' status to '{destJobStatus}' status")
                for job in jobs['idle']:
                    if condorrm:
                        remove_condor_job(jctrl, job, schedd)
                    set_job_status(jctrl, job, destJobStatus)

                if len(jobs['running']) > 0:
                    print(f"Setting {len(jobs['running'])} jobs with 'running' status to '{destJobStatus}' status")
                for job in jobs['running']:
                    if condorrm:
                        remove_condor_job(jctrl, job, schedd)
                    set_job_status(jctrl, job, destJobStatus)

